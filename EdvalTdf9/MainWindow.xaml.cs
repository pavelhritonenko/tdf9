﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using Tdf9Extraction.Recognizer;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;
using Path = System.IO.Path;

namespace EdvalTdf9
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        static MainWindow()
        {
            var anyKnownFormat = string.Join(";", KnownFiles.Select(x => x.Value));
            Filter = $"Any known format|{anyKnownFormat}|" +
                     $"{string.Join("|",  KnownFiles.Select(p => $"{p.Key}|{p.Value}"))}" +
                     $"|All Files|*.*";
        }

        public MainWindow()
        {
            InitializeComponent();

            ShowVersionInWindowTitle();

            PreviewKeyDown += CloseWindowIfEscapePressed;

            try
            {
                var folder = string.IsNullOrWhiteSpace(Properties.Settings.Default.StartupDirectory)
                    ? VisualExtensions.GetKnownFolder(VisualExtensions.KnownFolder.Downloads)
                    : Properties.Settings.Default.StartupDirectory;

                SelectLatestFileFromDirectory(folder);
            }
            catch (Exception ex)
            {
                SetError(ex.Message, ex);
            }
        }

        private void ShowVersionInWindowTitle()
        {
            var version = typeof(MainWindow).Assembly.GetName().Version.ToString();
            Title = $"Edval data converter (Tdf9/NovaT6 Options/CAS) {version}";
        }

        private void CloseWindowIfEscapePressed(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape) Close();
        }

        private static readonly Dictionary<string, string> KnownFiles =
            new Dictionary<string, string>
            {
                {"NovaT6", "*.xlsx"},
                {"Tdf9", "*.tdf9"},
                {"Cas File", "days.txt;classes.txt;scw.txt;schedule.txt;row.txt"},
                {"Zip Container", "*.zip"}
            };

        private static readonly string Filter;

        private static IEnumerable<string> GetSupportedFiles(string dir)
        {
            foreach (var p in KnownFiles)
            {
                foreach (var file in Directory.GetFiles(dir, p.Value))
                {
                    yield return file;
                }
            }
        }

        private void SelectLatestFileFromDirectory(string defaultStartupDirectory)
        {
            if (string.IsNullOrWhiteSpace(defaultStartupDirectory))
            {
                return;
            }

            if (!Directory.Exists(defaultStartupDirectory)) return;

            var file = GetSupportedFiles(defaultStartupDirectory)
                .OrderByDescending(File.GetLastWriteTimeUtc)
                .FirstOrDefault();

            if (file != null)
            {
                SelectFile(file);
            }
        }

        private async void OpenSourceFile_Click(object sender, RoutedEventArgs e)
        {
            await Task.FromResult(0);
            
            try
            {
                string sourceFileName;
                if (!SelectSourceFile(out sourceFileName)) return;

                SelectFile(sourceFileName);
            }
            catch (FileNotFoundException ex)
            {
                SetError($"File not found: {ex.FileName}", ex);
            }
            catch (Exception ex)
            {
                SetError(ex.Message, ex);
            }
        }

        private void SelectFile(string sourceFileName)
        {
            ClearError();
            SourceFileName.Text = sourceFileName;
            var outputFolder = Path.GetDirectoryName(sourceFileName);
            OutputFolder.Text = outputFolder;
            Properties.Settings.Default.StartupDirectory = outputFolder;
            Properties.Settings.Default.Save();
        }

        private void ClearError()
        {
            ConvertingError.Content = string.Empty;
            ConvertingError.ToolTip = string.Empty;
            ConvertingError.Visibility = Visibility.Hidden;
        }

        private void Convert(string sourceFileName, string outputFolder)
        {
            ConvertingError.Visibility = Visibility.Hidden;
            ConvertingResult.Visibility = Visibility.Hidden;

            var sb = new StringBuilder();
            var convertResult =
                TypeRecognizer.Extract(sourceFileName, outputFolder, x => sb.AppendLine(x.RenderMessage()));

            ClearAndShowConvertLog(sb.ToString());

            SetSuccessConvertingResult(convertResult);
            OpenResultFolder(convertResult);
        }

        private void ClearAndShowConvertLog(string log)
        {
            Log.Text = log;

            if (!string.IsNullOrWhiteSpace(log))
            {
                Log.Visibility = Visibility.Visible;

                if (Math.Abs(Height - MinHeight) < 1)
                {
                    Height = 400;
                }
            }
        }

        private bool SelectSourceFile(out string sourceFileName)
        {
            var dlg = new OpenFileDialog
            {
                FileName = "Select TDF9 file",
                Filter = Filter,
                Multiselect = false,
                CheckFileExists = true,
                CheckPathExists = true
            };

            var result = dlg.ShowDialog(this);

            sourceFileName = result == true ? dlg.FileName : null;

            return result ?? false;
        }

        private void SetError(string message, Exception exception)
        {
            ConvertingError.ToolTip = exception.ToString();
            ConvertingError.Content = message;
            ConvertingError.Visibility = Visibility.Visible;
        }

        private void SetSuccessConvertingResult(string convertResult)
        {
            ResultFile.Visibility = Visibility.Visible;
            ConvertingResult.Visibility = Visibility.Visible;
            ConvertingResult.Text = convertResult;
        }

        private void SelectOutputFolder_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new FolderBrowserDialog();
            var result = dlg.ShowDialog(this.GetIWin32Window());
            if (result == System.Windows.Forms.DialogResult.OK)
                OutputFolder.Text = dlg.SelectedPath;
        }

        private void ResultFile_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            OpenResultFolder(ConvertingResult.Text);
        }

        private static void OpenResultFolder(string folder)
        {
            Process.Start("explorer.exe", $"\"{folder}\"");
        }

        private void ConvertBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(SourceFileName.Text))
                    Convert(SourceFileName.Text, OutputFolder.Text);
            }
            catch (Exception ex)
            {
                SetError(ex.Message, ex);
            }
        }
    }
}
