#r @"packages\FAKE\tools\FakeLib.dll"
#r @"packages\FAKE\tools\Fake.Experimental.dll"
#r "System.Management.Automation"

open Fake
open Fake.AssemblyInfoFile
open System.Management.Automation

let config = getBuildParamOrDefault "configuration" "Release"
let platform = getBuildParamOrDefault "platform" "AnyCPU"

let release =
    ReadFile "release_notes.md"
    |> ReleaseNotesHelper.parseReleaseNotes

Target "Generate AssemblyInfo" {

    CreateCSharpAssemblyInfo "EdvalTdf9/Properties/AssemblyInfo.cs"
      [ Attribute.Title "Edval Tdf9 extractor"
        Attribute.Product "Edval Tdf9 extractor"
        Attribute.Description "Tool for extract data from TDF9 format and store it in CSV files"
        Attribute.Version release.AssemblyVersion
        Attribute.FileVersion release.AssemblyVersion]
}

Target "Set BuildNumber" {
    let now =
        System.DateTime.UtcNow.ToString("MMMddHHmm", System.Globalization.CultureInfo.InvariantCulture)
            .ToLower()

    PowerShell.Create()
        .AddScript(sprintf "Update-AppveyorBuild -Version %s-%s" release.AssemblyVersion now)
        .Invoke()
    |> ignore
}

Target "Build" {
    MSBuildHelper.build (fun p ->
        { p with Verbosity = Some MSBuildVerbosity.Quiet
                 Targets = [ "Build" ]
                 Properties = [ "Configuration", config ] }) "tdf9-extraction.sln"
}

"Set BuildNumber" ==> "Generate AssemblyInfo" ==> "Build"

RunTargetOrDefault "Build"