﻿using System;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;
using tdf9.tests.Samples;
using Tdf9Extraction;
using Tdf9Extraction.Recognizer;

namespace tdf9.tests
{
    public sealed class DetectionTests
    {
        [TestCase("cas.zip")]
        [TestCase("cas")]
        [TestCase("cas/days.txt")]
        public void CasDetectionTests(string fileName)
        {
            Assert.IsNotNull(
                TypeRecognizer.Recognize(new Sample(fileName).FullPath).CasSource,
                "Expected Cas format");
        }

        [TestCase("NovaT6.xlsx")]
        public void NovaT6Detection(string fileName)
        {
            Assert.IsNotNull(
                TypeRecognizer.Recognize(new Sample(fileName).FullPath).NovaT6,
                "Expected NovaT6 format");
        }

        [TestCase("master timetable 2016-v9.zip")]
        [TestCase("2016 Timetable Term 3.tdf9")]
        public void Tdf9DetectionTests(string fileName)
        {
            var result = TypeRecognizer.Recognize(new Sample(fileName).FullPath);
            
            Assert.IsNotNull(result.Tdf9Source, "Expecte TDF9 format");
        }

        [Test]
        public void StartRecognize()
        {
            var input = "     100000 001P01 Religion Plan    ";
            CasParser.ParseRowStart(input).Iter(Console.WriteLine);
        }

        public static IEnumerable<object[]> ParseTeacherAndRoomsTestsCases()
        {
            yield return new object[]
            {
                "   Parisi, Mr.                      104,106,112   ",
                Tuple.Create("Parisi, Mr.", new[] {"104", "106", "112"})
            };
            yield return new object[]
            {
                "   104  ",
                Tuple.Create("", new[] {"104" })
            };
            yield return new object[]
            {
                "   Parisi, Mr. ",
                Tuple.Create("Parisi, Mr.", new string[0])
            };
            yield return new object[]
            {
                "         Stewart, John     ",
                Tuple.Create("Stewart, John", new string[0])
            };
        }

        [TestCaseSource(nameof(ParseTeacherAndRoomsTestsCases))]
        public void ParseTeacherAndRoomsTests(string input, Tuple<string, string[]> result)
        {
            using (var stream = new MemoryStream())
            {
                var rooms = CasParser.ParseDays(() => File.OpenRead(new Sample("cas/days.txt").FullPath),
                    () => new StreamWriter(stream));

                var actual = CasParser.ParseTeacherAndRooms(input, rooms);
                Assert.AreEqual(result.Item1, actual.Item1);
                Assert.AreEqual(result.Item2, actual.Item2);
            }
        }
    }
}