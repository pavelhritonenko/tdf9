﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using NUnit.Framework;
using Serilog;
using tdf9.tests.Samples;
using Tdf9Extraction.NovaT6;
using Tdf9Extraction.Recognizer;

namespace tdf9.tests
{
    [TestFixture]
    public class novat6_tests
    {
        private readonly Serilog.Core.Logger _logger = new LoggerConfiguration().WriteTo.Console().CreateLogger();
        private readonly INovaT6 _content;

        public novat6_tests()
        {
            _content = NovaT6.FromFile(new Sample("NovaT6.xlsx").FullPath);
            Dump(_content.ReadLines());
        }

        private static void Dump(IEnumerable<string> content)
        {
            foreach (var line in content)
            {
                Console.WriteLine(line);
            }
        }

        [Test]
        public void ReadAsLines()
        {
            Assert.IsNotEmpty(_content.ReadLines());
            Assert.IsTrue(_content.ReadLines().Contains("Zhuwao, Chantel (9H)"));
        }

        [TestCase("Armstrong, Ellie-May (9T)")]
        [TestCase("Ashburn, Martha (9D)")]
        [TestCase("Atkinson, Erin (9T)")]
        [TestCase("Bage, Eleanor (9S)")]
        [TestCase("Bannister, Ella (9A)")]
        [TestCase("Bartolome, Gieneva (9S)")]
        [TestCase("Beaton, Lucy (9T)")]
        [TestCase("Begum, Asia (9H)")]
        [TestCase("BEGUM, Syeda (9C)")]
        [TestCase("Begum, Syeda (9S)")]
        public void parse_student_name(string nameline)
        {
            StudentName sn;
            Assert.IsTrue(StudentName.TryParse(nameline, 42, out sn), $"expected `{nameline}` to be parsed");
            Console.WriteLine($"{nameline} -> {JsonConvert.SerializeObject(sn)}");
        }

        [TestCase("1    Gg        Geography           ", 1, "Gg", "Geography", false)]
        [TestCase("2    Bp        Btec Sports         ", 2, "Bp", "Btec Sports", false)]
        [TestCase("3    Ad        Art & Design        ", 3, "Ad", "Art & Design", false)]
        [TestCase("R1   Bh        Btec H&SC           ", 1, "Bh", "Btec H&SC", true)]
        [TestCase("R2   Fd        Food Studies        ", 2, "Fd", "Food Studies", true)]
        public void parse_preference_line(string line, int order, string code, string name, bool isReserved)
        {
            Preference p;
            Assert.IsTrue(Preference.TryParse(line, out p));
            Console.WriteLine($"{line} -> {JsonConvert.SerializeObject(p)}");
            
            Assert.AreEqual(order, p.Order);
            Assert.AreEqual(code, p.CourseCode);
            Assert.AreEqual(name, p.CourseName);
            Assert.AreEqual(isReserved, p.IsReserved);
        }

        [Test]
        public void total_entries_should_be_the_same_as_names()
        {
            StudentName _;
            var namesCount = _content.ReadLines().Count(x => StudentName.TryParse(x, 42, out _));
            var foldedCount = StudentPreferences.Parse(_content.ReadLines(), _logger).Count();
            
            Assert.AreEqual(namesCount, foldedCount);
        }

        [Test]
        public void student_preferences_rows_should_be_same_as_students_count()
        {
            StudentName _;
            var namesCount = _content.ReadLines().Count(x => StudentName.TryParse(x, 42, out _));

            var prefs = NovaT6.RenderStudentPrefs(StudentPreferences.Parse(_content.ReadLines(), _logger).ToArray()).ToArray();
            
            Dump(prefs);
            
            Assert.AreEqual(namesCount + 1, prefs.Count());
            Assert.IsTrue(prefs.Contains("E-0153,\"O'BRIEN, Amber\",Hi,Pd,Ce,Ab,Gg,,4,1"));
            Assert.IsTrue(prefs.Contains("E-0009,\"ALLAN, Serena\",Hi,Bh,Fd,,Ab,,3,1"));
        }

        [Test]
        public void elective_courses_are_sorted_by_code()
        {
            var lines = NovaT6.RenderElectiveCourses(StudentPreferences.Parse(_content.ReadLines(), _logger).ToArray());

            var groups = lines.Skip(1).Select(x =>
            {
                var parts = x.Split(',');
                return new {Group = parts[1], Year = parts[0]};
            }).GroupBy(x => x.Year);

            foreach (var g in groups)
            {
                var codes = g.Select(x => x.Group).ToArray();
                var sorted = codes.OrderBy(x => x);
                Assert.AreEqual(codes, sorted);
            }
        }
    }
}