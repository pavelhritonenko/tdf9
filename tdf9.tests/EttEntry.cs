using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using NUnit.Framework;

namespace tdf9.tests
{
    public sealed class EttEntry
    {
        public EttEntry(string code, Dictionary<string, string> fields)
        {
            Code = code;
            Fields = fields;
        }

        public string Code { get; }
        public Dictionary<string, string> Fields { get; }

        public static bool TryParse(string line, out EttEntry entry)
        {
            var match = Regex.Match(line, @"^\s*(?<code>\S+)\s+(?<rest>.*)$");
            if (!match.Success)
            {
                entry = null;
                return false;
            }
            entry = ProcessMatch(match);
            
            return entry.Fields.Any();
        }

        private static EttEntry ProcessMatch(Match match)
        {
            var code = match.Groups["code"].Value;
            var rest = match.Groups["rest"].Value;
            var matches = Regex.Matches(rest, @"(?<key>\w+)\=((?<quoted>\""(?<qvalue>[^\""]*)\"")|(?<nonquoted>[^\s]*))");
            var fields = matches.Cast<Match>().ToDictionary(
                m => m.Groups["key"].Value,
                m => m.Groups["quoted"].Success ? m.Groups["qvalue"].Value : m.Groups["nonquoted"].Value);
            return new EttEntry(code, fields);
        }

        public static EttEntry Parse(string line)
        {
            var match = Regex.Match(line, @"^\s*(?<code>[^\s]+)\s+(?<rest>.*)$");
            Assert.IsTrue(match.Success, "Wrong line format");
            var entry = ProcessMatch(match);

            Assert.IsTrue(entry.Fields.Any(), $"Haven't found any field for {line}");                
            
            
            return entry;
        }
    }
}