﻿##New in 1.1.5.0 (Release 2017/10/27)

* Formats recognition improved
* Bug: NullReferenceException on recognition fixed

##New in 1.1.4.0 (Release 2017/10/24)

* Log of NovaT6 extraction is shown when extraction is complete

##New in 1.1.3.0 (Release 2017/10/22)

* Serilog as a logging framework, put warnings to output file
* Simple UI-tweaks
* Elective courses: Student year -> Yeaer
* Groups in Elective courses sorted by code
* More convient order for formats in open source dialog

##New in 1.1.2.0 (Release 2017/10/19)

* Detection of default file to convert fixed

##New in 1.1.1.0 (Release 2017/10/18)

* Main/Reserved options aligned accordingly

##New in 1.1.0.1 (Release 2017/10/16)

* NovaT6 student preferences parsed and exported
* NovaT6 elective courses file exported

## New in 1.0.42.1 (Release 2017/09/09)

* Elective sections are rendered
* Exported faculties are in intersection of sets of classes/teachers

## New in 1.0.41.0 (Release 2017/09/04)

* Parameters are rendered

## New in 1.0.40.3 (Release 2017/08/28)

* Period references in classes section
* Version format is always 9.9.999.9
* Actual EdvalClassCodes in users section
* Student emails exported

## New in 1.0.39.1 (Release 2017/08/24)

* Render native ett-format

## New in 1.0.38.3 (Released 2017/06/03)

* All preferences parsed per year as much as all

## New in 1.0.38.2 (Released 2017/06/03)

* ConstraintOptions: Look up both columns, and move the ID columns to the right

## New in 1.0.38.1 (Released 2017/06/03)

* Options collection enriched by subject

## New in 1.0.38.0 (Released 2017/06/03)

* Sof9 files parsed separately
* Preferences enriched by Option's Name/Code, Student's fullname
* Student's fullname parsing fixed

## New in 1.0.37.0 (Released 2017/05/31)

* Sof9 files parsing
* Hyphen in teacher unavailable field
* Student's and Teacher's first name in TitleCase
* TTable.csv and TTableEdvalClassCodes.csv now have Room and Teacher fields, LookupReference always creates field in target entry
* Students preferred name is optional (dot-convention)
* Escape closes window
* Format order checking changed
* Small refactoring
* Fixes in headers, parsing some compound fields to arrays
* row.txt parsing
* Simple files parsing
* Correct output folder
* Both formats (CaS and Tdf9) are recognized
* ISSUE 68 - Year and Spread for classes that have null Year
* ISSUE 67 - Spead typo fix and column order for classes.csv
* ISSUE 69 - fail on master timetable 2016-v9.zip sample
* Issue 66 - number of doubles
* MRCG info moved to classes
* AppVeyor build script
* Issue 63 fix - columns order in teachers.csv
* Issue 62 implemented
* Issues 55-61
* Issues 39-54
* Issues 25-38 fixed
* More less common files
* MCRGs parsing, convert error handling
* Yard duties
* Several collections enriched
* Published dates parsing
* Some UI changes, Unscheduled Duties lookup
* Student classes with dots
* Rooms unavailability
* Teacher unavailability
* [WIP] 4 issues implemented
* Collection enrichments and Last Folder memoizing
* Icon, Removed RollClass from timetable.csv
* Additional lookups for periods and rollclasses, timetable separated to two files
* FW dlls exluded from bundling
* Collection lookups and FullName field for Teachers and Students collections
* Flow and layout changed
* Costura.Fody used for bundling
* Small refactoring
* Pretty XML files support implemented
* Windows application that supports FlatOPC type
* Alternative (simplier) parsing
* initial commit
