using System;
using System.IO;

namespace Tdf9Extraction.Recognizer
{
    public class DiskTdf9Source : ITdf9Source
    {
        private readonly string _fileName;

        public DiskTdf9Source(string fileName)
        {
            _fileName = fileName;
        }

        public Tdf9Container Open()
        {
            return new Tdf9Container(File.OpenRead(_fileName), new Tuple<Stream, string>[0]);
        }

        public void Dispose()
        {
        }
    }
}