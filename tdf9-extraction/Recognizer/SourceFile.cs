﻿using System;

namespace Tdf9Extraction.Recognizer
{
    public sealed class SourceFile
    {
        public ICasSource CasSource { get; }

        public SourceFile(ICasSource casSource)
        {
            if (casSource == null) throw new ArgumentNullException(nameof(casSource));
            CasSource = casSource;
        }

        public SourceFile(ITdf9Source tdf9)
        {
            if (tdf9 == null) throw new ArgumentNullException(nameof(tdf9));
            Tdf9Source = tdf9;
        }

        public ITdf9Source Tdf9Source { get; }
        
        public UnknownFormat UnknownFormat { get; }
        public INovaT6 NovaT6 { get; }

        public SourceFile(UnknownFormat unknownFormat)
        {
            if (unknownFormat == null) throw new ArgumentNullException(nameof(unknownFormat));
            UnknownFormat = unknownFormat;
        }

        public SourceFile(INovaT6 novaT6)
        {
            if (novaT6 == null) throw new ArgumentNullException(nameof(novaT6));
            NovaT6 = novaT6;
        }
    }
}