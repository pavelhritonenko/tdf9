namespace Tdf9Extraction.Recognizer
{
    public sealed class UnknownFormat
    {
        public string Message { get; private set; }

        public UnknownFormat(string message)
        {
            Message = message;
        }
    }
}