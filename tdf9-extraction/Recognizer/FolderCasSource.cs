using System;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using Serilog.Events;

namespace Tdf9Extraction.Recognizer
{
    public class FolderCasSource : ICasSource
    {
        private readonly string _folder;

        public FolderCasSource(string folder)
        {
            _folder = folder;
        }

        private FileStream OpenFile(string fileName)
        {
            var allFiles = Directory
                    .GetFiles(_folder)
                    .Select(x => new
                    {
                        FileName = Path.GetFileName(x).ToLower(),
                        FullName = x
                    }).ToArray();

            return File.OpenRead(allFiles.SingleOrDefault(x => x.FileName == fileName)?.FullName);
        }

        public Stream OpenClasses()
        {
            return OpenFile("classes.txt");
        }

        public Stream OpenDays()
        {
            return OpenFile("days.txt");
        }

        public Stream OpenRow()
        {
            return OpenFile("row.txt");
        }

        public Stream OpenSchedule()
        {
            return OpenFile("schedule.txt");
        }

        public Stream OpenScw()
        {
            return OpenFile("scw.txt");
        }

        public string CreateOutputDirectory(string outputFolder)
        {
            var folder = "out".Yield()
                .Concat(Enumerable.Range(1, int.MaxValue).Select(x => $"out-{x}"))
                .Select(x => Path.Combine(outputFolder ?? _folder, x))
                .First(candidate => !File.Exists(candidate) && !Directory.Exists(candidate));
            return Directory.CreateDirectory(folder).FullName;
        }

        public void Dispose()
        {
        }

        public IObservable<LogEvent> Log => Observable.Never<LogEvent>();

        public string Extract(string fileOrFolder, string outDir)
        {
            var dir = CreateOutputDirectory(outDir);
            CasParser.Extract(this, dir);
            return dir;
        }
    }
}