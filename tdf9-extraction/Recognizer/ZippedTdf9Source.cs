﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;

namespace Tdf9Extraction.Recognizer
{
    public class ZippedTdf9Source : ITdf9Source
    {
        private readonly FileStream _stream;
        private readonly ZipArchive _zip;

        public ZippedTdf9Source(string filePath)
        {
            _stream = File.OpenRead(filePath);
            _zip = new ZipArchive(_stream);
        }

        public Tdf9Container Open()
        {
            var main = _zip
                .Entries
                .First(x => x.FullName.EndsWith(".tdf9", StringComparison.InvariantCultureIgnoreCase))
                .Open();

            var options = _zip.Entries.Where(x =>
                    x.FullName.EndsWith(".sof9", StringComparison.InvariantCultureIgnoreCase))
                .OrderByDescending(x => x.Length)
                .Select(x => Tuple.Create(x.Open(), Path.GetFileNameWithoutExtension(x.FullName)))
                .ToArray();

            return new Tdf9Container(main, options);
        }

        public void Dispose()
        {
            _zip.Dispose();
            _stream.Dispose();
        }
    }
}