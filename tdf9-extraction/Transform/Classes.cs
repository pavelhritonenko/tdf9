﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Tdf9Extraction.Transform
{
    public sealed class Classes
    {
        public static void RenameWithDotNotation(IEnumerable<Dictionary<string, string>> classes)
        {
            classes.Iter(entry =>
            {
                entry["Code"] = Regex.Replace(entry.GetOrDefault("Code", ""), @"\s+", ".");
            });
        }
    }
}
